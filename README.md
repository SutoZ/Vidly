# FormulaRace
> FormulaRace is an ASP.NET MVC web application project which adds CRUD functionality to customers and movied that can be rent.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
You can see the customers and movies seeded from the database and you can also add new members. I have chosen C# for server-side development and razor, jQuery for client-side development.

## Technologies
* ASP.NET MVC
* EntityFramework 6.1.3
* AutoMapper 4.1.0
* bootstrap 3.0.0
* jQuery 3.3.1
* Micorosft.AspNet.Razor 3.2.7
* Micorosft.Owin 4.0.0
* NewtonSoft.Json 11.0.2
* SimpleInjector 2.5.0
* T4MVC 4.2.4

## Setup

``` dotnet restore ``` \
``` update database ```

## Features
CRUD operations


To-do list:
* Thread safety
* Add movies to customers

## Status
Project is: _in progress_

## Contact
Created by [@SutoZ]