namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeInTable : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.GenreDtoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GenreDtoes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
