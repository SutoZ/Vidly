namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerTableAddColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "BirthDate", c => c.DateTime());
            AddColumn("dbo.Customers", "IsSubscribedToNewsletter", c => c.Boolean(nullable: false));            
        }

        public override void Down()
        {
            DropColumn("dbo.Customers", "BirthDate");
            DropColumn("dbo.Customers", "IsSubscribedToNewsletter");


        }
    }
}
