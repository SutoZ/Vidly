namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMyCustomerNameProperty : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MyCustomers", "Name", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MyCustomers", "Name", c => c.String());
        }
    }
}
