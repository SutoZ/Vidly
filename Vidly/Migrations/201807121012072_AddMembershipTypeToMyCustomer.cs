namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipTypeToMyCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyCustomers", "MembershipTypeId", c => c.Byte(nullable: false));
            CreateIndex("dbo.MyCustomers", "MembershipTypeId");
            AddForeignKey("dbo.MyCustomers", "MembershipTypeId", "dbo.MembershipTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MyCustomers", "MembershipTypeId", "dbo.MembershipTypes");
            DropIndex("dbo.MyCustomers", new[] { "MembershipTypeId" });
            DropColumn("dbo.MyCustomers", "MembershipTypeId");
        }
    }
}
