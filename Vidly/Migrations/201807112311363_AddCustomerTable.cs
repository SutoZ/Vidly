namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomerTable : DbMigration
    {
        public override void Up()
        {
            CreateTable("dbo.Customers", c => new
            {
                Id = c.Int(nullable: false, identity: true),
                Name = c.String(),
            }).PrimaryKey(t => t.Id);

            AlterColumn("dbo.Customers", "Name", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            DropTable("dbo.Customers");

            AlterColumn("dbo.Customers", "Name", c => c.String(nullable: false, maxLength: 250));
        }
    }
}
