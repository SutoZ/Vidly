namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGenre : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO GENRES (ID, NAME) VALUES (1, 'Comedy')");
            Sql("INSERT INTO GENRES (ID, NAME) VALUES (2, 'Romance')");
            Sql("INSERT INTO GENRES (ID, NAME) VALUES (3, 'Action')");
            Sql("INSERT INTO GENRES (ID, NAME) VALUES (4, 'Advanture')");
        }
        
        public override void Down()
        {
        }
    }
}
