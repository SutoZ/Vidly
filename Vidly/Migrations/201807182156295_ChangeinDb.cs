namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeinDb : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.MembershipTypeDtoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MembershipTypeDtoes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
