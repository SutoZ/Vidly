namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BirthdayAddedToMyCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyCustomers", "Birthday", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyCustomers", "Birthday");
        }
    }
}
