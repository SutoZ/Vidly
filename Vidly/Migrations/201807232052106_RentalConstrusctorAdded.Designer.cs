// <auto-generated />
namespace Vidly.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class RentalConstrusctorAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(RentalConstrusctorAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201807232052106_RentalConstrusctorAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
