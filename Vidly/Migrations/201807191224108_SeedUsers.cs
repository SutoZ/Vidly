namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c619abc9-652c-4f0a-ab57-30d483ab5765', N'admin@vidly.com', 0, N'ACAlHO4W8FWGZkkTdRNS+29f9v9jrm1dbY5Y7MbOXM6LK9BOVwQJ/srpUe6WJYtl9w==', N'ad94003b-c37c-47d4-a2ad-9a285d5a588f', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'cf5bc96e-85ee-4a7b-886b-b498a1d03e6d', N'guest@vidly.com', 0, N'AFqgHB6K02msmIR8cS1HSKHJ/WTEwb5W2hiSVMyp3/G9qwV4kNCAFNoRch3sfV/Niw==', N'12f5d211-eacc-429f-ab9b-839925d9b3b8', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
                    
                    INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'48cd91de-d921-4134-9064-1193d19a4e51', N'CanManageMovies')

                    INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c619abc9-652c-4f0a-ab57-30d483ab5765', N'48cd91de-d921-4134-9064-1193d19a4e51')
                    INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cf5bc96e-85ee-4a7b-886b-b498a1d03e6d', N'48cd91de-d921-4134-9064-1193d19a4e51')   
");
        }

        public override void Down()
        {
        }
    }
}
