namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScubscribedToMyCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyCustomers", "IsSubscribedToNewsLetter", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyCustomers", "IsSubscribedToNewsLetter");
        }
    }
}
