﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using Vidly.Dtos;
using System.Data.Entity;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            this._context = new ApplicationDbContext();
        }

        //GET /api/Customers        
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetCustomers(string query = null)
        {
            var customersQuery = _context.MyCustomers.Include(c => c.MembershipType);

            if (!String.IsNullOrWhiteSpace(query))
                customersQuery = customersQuery.Where(c => c.Name.Contains(query));

            var customerDTos = customersQuery
                .ToList()
                .Select(Mapper.Map<MyCustomer, MyCustomerDto>);

           return Ok(customerDTos);              
        }
         
        //GET/api/Customers/1        
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _context.MyCustomers.SingleOrDefault(c => c.ID == id);
            if (customer == null)
                return NotFound();
            else return Ok(Mapper.Map<MyCustomer, MyCustomerDto>(customer));
        }

        //POST/api/Customers
        [System.Web.Http.HttpPost]
        //[System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult CreateCustomer(MyCustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                var customer = Mapper.Map<MyCustomerDto, MyCustomer>(customerDto);

                _context.MyCustomers.Add(customer);
                _context.SaveChanges();

                customerDto.ID = customer.ID;

                return Created(new Uri(Request.RequestUri + "/" + customer.ID), customerDto);
            }
        }

        //PUT/api/Customers/id
        [System.Web.Http.HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public void UpdateCustomer(int id, MyCustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var customerInDB = _context.MyCustomers.SingleOrDefault(c => c.ID == id);

            if (customerInDB == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(customerDto, customerInDB);

            _context.SaveChanges();
        }

        //DELETE /api/Customers/1

        [System.Web.Http.HttpDelete]
        //[System.Web.Mvc.AcceptVerbs(HttpVerbs.Delete)]
        [System.Web.Http.Route("api/customers/{id}")]
        [System.Web.Mvc.Authorize(Roles = RoleName.CanManageMovies)]
        public void DeleteCustomer(int id)
        {
            var customerInDB = _context.MyCustomers.SingleOrDefault(c => c.ID == id);

            if (customerInDB == null)            
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.MyCustomers.Remove(customerInDB);
            _context.SaveChanges();

        }
    }
}
