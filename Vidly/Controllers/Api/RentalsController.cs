﻿using System;
using System.Linq;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class RentalsController : ApiController
    {
        private ApplicationDbContext _context;

        public RentalsController()
        {
            this._context = new ApplicationDbContext();
        }

        //api/rentals/
        [HttpPost]
        public IHttpActionResult CreateNewRental(RentalsDto rentalsDto)
        {
            var customer = _context.MyCustomers.SingleOrDefault(c => c.ID == rentalsDto.MyCustomerId);

            var movies = _context.Movies.Where(m => rentalsDto.MovieId.Contains(m.Id)).ToList();

            int rental_id = 0;
            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                    return BadRequest("Movie not available!");

                movie.NumberAvailable--;

                var rental = new Rental()
                {
                    MyCustomer = customer,
                    Movie = movie,
                    DateRented = DateTime.Now
                    
                };
                rental_id = rental.Id;
                _context.Rentals.Add(rental);
            }
            
                _context.SaveChanges();
           
            //return Ok();
            return Created(new Uri(Request.RequestUri + "/" + rental_id), rentalsDto);

        }
    }
}
