﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;


namespace Vidly.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController()
        {
            this._context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [OutputCache(Duration = 0, VaryByParam = "*", NoStore = true)]
        public ActionResult Index()
        {
            /*   if (MemoryCache.Default["Genres"] == null)
               {
                   MemoryCache.Default["Genres"] = _context.Genres.ToList();
               }

               var genres = MemoryCache.Default["Genres"] as IEnumerable<Genre>;
               */

            if (User.IsInRole(RoleName.CanManageMovies))
                return View("List");


            return View("ReadOnlyList");
        }

        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult New()
        {
            var membershipTypes = _context.MembershipTypes.ToList();

            var viewModel = new CustomerFormViewModel()
            {
                MembershipTypes = membershipTypes,
                MyCustomer = new MyCustomer()
            };
            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Save(MyCustomer myCustomer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new CustomerFormViewModel
                {                    
                    MyCustomer = myCustomer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("CustomerForm", viewModel);
            }

            if (myCustomer.ID == 0)
            {
                _context.MyCustomers.Add(myCustomer);
            }
            else
            {
                
                var customerInDB = _context.MyCustomers.Single(c => c.ID == myCustomer.ID);                

                customerInDB.IsSubscribedToNewsLetter = myCustomer.IsSubscribedToNewsLetter;
                customerInDB.MembershipTypeId = myCustomer.MembershipTypeId;
                customerInDB.Name = myCustomer.Name;                
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Customer");
        }

        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Edit(int id)
        {
            var customer = _context.MyCustomers.SingleOrDefault(c => c.ID == id);

            if (customer == null)
            {
                return HttpNotFound();
            }

            var viewModel = new CustomerFormViewModel()
            {
                MyCustomer = customer,
                MembershipTypes = _context.MembershipTypes
            };

            return View("CustomerForm", viewModel);
        }

        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Details(int id)
        {
            var customers = _context.MyCustomers.Include(c => c.MembershipType)
                .SingleOrDefault(c => c.ID == id);

            if (customers == null)
            {
                return HttpNotFound();
            }
            else
                return View(customers);

        }
    }
}