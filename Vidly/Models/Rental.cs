﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Rental
    {

        public Rental()
        {
            this.DateRented = new DateTime(2017,10,10);
            this.DateReturned = DateTime.Now;
        }
        public int Id { get; set; }

        [Required]
        public MyCustomer MyCustomer { get; set; }

        [Required]
        public Movie Movie { get; set; }

        [Display(Name = "Date Rented")]
        [Column(TypeName = "Datetime2")]
        public DateTime? DateRented { get; set; }

        [Display(Name = "Date Returned")]
        public DateTime DateReturned { get; set; }
    }
}