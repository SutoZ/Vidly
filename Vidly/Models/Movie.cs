﻿using System;
using System.ComponentModel.DataAnnotations;
using Vidly.Dtos;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public Genre Genre { get; set; }
            
        [Display(Name = "Genre")]
        [Required(ErrorMessage = "The Genre field is required.")]
        public byte GenreId { get; set; }

        [Display(Name = "Release Date")]
        [Required(ErrorMessage = "The Release Date field is required.")]
        public DateTime ReleaseDate { get; set; }
        public DateTime DateAdded { get; set; }

        [Display(Name = "Number in Stock")]
        [Required(ErrorMessage = "The Number in Stock field is required.")]
        [Range(1, 20, ErrorMessage = "The field number in Stock must be between 1 and 20.")]
        public int NumberInStock { get; set; }

        public byte NumberAvailable { get; set; }
    }
}