﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Min18YearsIfMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var myCustomer = (MyCustomer)validationContext.ObjectInstance;

            if (myCustomer.MembershipTypeId == MyCustomer.Unknown
                || myCustomer.MembershipTypeId == MyCustomer.Pay_As_You_GO)
                return ValidationResult.Success;

            if (myCustomer.Birthday == null)
            {
                return new ValidationResult("Birthday is required!");
            }

            var age = DateTime.Today.Year - myCustomer.Birthday.Value.Year;

            return (age >= 18)
                ? ValidationResult.Success
                : new ValidationResult("Customer should be at least 18 years old to go on a membership.");
        }
    }
}