﻿using System;
using System.ComponentModel.DataAnnotations;
using Vidly.Dtos;

namespace Vidly.Models
{
    public class MyCustomer
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Please enter customer's name")]
        [StringLength(255)]
        public string Name { get; set; }
        public bool IsSubscribedToNewsLetter { get; set; }
        public MembershipType MembershipType { get; set; }

        [Display(Name = "Membership Type")]
        public byte MembershipTypeId { get; set; }

        [Min18YearsIfMember]
        [Display(Name = "Date of Birth")]
        public DateTime? Birthday { get; set; }

        public static readonly byte Unknown = 0;
        public static readonly byte Pay_As_You_GO = 1;
    }
}