﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class RentalsDto
    {
        public int MyCustomerId { get; set; }
        public List<int> MovieId { get; set; }
    }
}