﻿using System.Collections.Generic;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public MyCustomer MyCustomer { get; set; }
        public string Title
        {
            get
            {
                if (MyCustomer == null || MyCustomer.ID == 0)
                {
                    return "New Customer";
                }
                else return "Edit Customer";
            }
        }
    }
}